<?php get_header(); ?>
<section id="content" role="main">
<article id="post-0" class="post not-found">
<header class="header">
<h1 class="entry-title"><?php _e( qtranxf_use(qtrans_getLanguage(), get_field('404_title', 'option'), false) , 'blankslate' ); ?></h1>
<div id="search">
	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	    <label>
	        <input type="search" class="header_spaced search-field" placeholder="<?php echo esc_attr_x( qtranxf_use(qtrans_getLanguage(), get_field('404_try_something_else', 'option'), false) , 'placeholder' ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" focustext="<?php echo esc_attr_x(qtranxf_use(qtrans_getLanguage(), get_field('search_text_hover', 'option'), false), 'placeholder_focus'); ?>" />
	    </label>
	    <input hidden="true" type="submit" class="search-submit"
	        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
	</form>
</div>
</header>

<h3 class="search_not_found"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('404_see_more_posts', 'option'), false); ?></h3>

<div id="all_projects" class="all_search">
	<div class="projects_gutter"></div>
		<?php echo get_the_isotope_posts(array('post_type'=>array('post', 'project'), 'orderby'=>'rand'), 'search'); ?>
</div>

</article>

</section>
<?php get_footer(); ?>