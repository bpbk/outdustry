<?php get_header(); ?>
<section id="content" role="main" posttype="roster">
	<div id="page_sub_header">
		<div id="page_sub_header_container">
			<?php $roster_page = get_page_by_title('roster'); ?>
			<?php $page_title = $roster_page->post_name; ?>
			<h6 class="brackets main_page_title"><?php echo $page_title; ?></h6>
			<div class="page_sub_header_content">
				<?php print apply_filters('the_content', $roster_page->post_content); ?>
			</div>
		</div>
	</div>
	<div id="all_projects_container">
		<h5 class="header_spaced mobile filter_by header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('filter_by', 'option'), false); ?></h5>
		<div id="filter_boxes">
			<div id="filter_box_country" class="filter_box">
				<?php $theTerm = get_taxonomy('country'); ?>
				<div class="filter_box_inner">
					<div class="filter_box_content filter_header" slug="country">
						<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
						<div class="filter_arrow arrow_small">
							<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
						</div>
					</div>
					<?php $terms = get_terms(array('taxonomy' => 'country' )); ?>
						<?php if(count($terms) > 0){ ?>
							<div class="filter_list">
								<?php foreach($terms as $term){ ?>
									<div class="filter_item country_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="country">
										<div class="filter_item_content">
											<div class="filter_checkbox"></div>
											<p><?php echo $term->name; ?></p>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
				</div>
			</div>
			<div id="filter_box_skills" class="filter_box">
				<?php $theTerm = get_taxonomy('skills'); ?>
				<div class="filter_box_inner">
					<div class="filter_box_content filter_header" slug="skills">
						<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
						<div class="filter_arrow arrow_small">
							<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
						</div>
					</div>
					<?php $terms = get_terms(array('taxonomy' => 'skills' )); ?>
						<?php if(count($terms) > 0){ ?>
							<div class="filter_list">
								<?php foreach($terms as $term){ ?>
									<div class="filter_item skills_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="skills">
										<div class="filter_item_content">
											<div class="filter_checkbox"></div>
											<p><?php echo $term->name; ?></p>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
				</div>
			</div>
		</div>
		<div id="active_filter_items">
		</div>
		<div id="all_projects" class="all_roster">
			<div class="projects_gutter"></div>
				<?php echo pre_isotope_posts(1, 'roster'); ?>
		</div>
	<?php 
		/*$args = array('post_type' => 'roster');
		$roster_query = new WP_Query( $args ); 
		if ( $roster_query->have_posts() ) : ?>
		<div id="all_projects" class="all_roster">
			<div class="projects_gutter"></div>
			<?php while ( $roster_query->have_posts() ) : $roster_query->the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="archive_post_container">
					<div class="archive_post_image bg_centered" style="background-image:url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>)">
						<a href="<?php echo get_the_permalink(); ?>"></a>
					</div>
					<div class="archive_post_content_container">
						<div class="archive_post_content">
							<div class="archive_posts=_content_inner">
								<h3 class="entry-title roster-title">
									<span>
										<a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a>
									</span>
								</h3>
								<?php $format = get_the_terms(get_the_id(), 'format'); ?>
								<?php if(count($format) > 0){ ?>
									<h4 class="roster_sub_header"><span>Lorem Ipsum</span></h4>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
		<?php wp_reset_postdata();
			endif; ?>*/
			?>
	</div>
</section>
<script>
	
</script>
<?php get_footer(); ?>