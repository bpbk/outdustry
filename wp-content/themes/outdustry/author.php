<?php get_header(); ?>
<section id="content" role="main">
<header class="header">
<?php the_post(); ?>
<?php $auth = $wp_query->get_queried_object();  ?>

<h6 class="brackets main_page_title"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('title_team', 'option'), false); ?></h6>

<div class="project_container">
	<div class="project_container_inner">
		<div class="project_media">
			<?php $authImg = get_field('full_image'); ?>
			<?php if ( $authImg ) { ?>
				<div id="post_featured_image">
		 			<img src="<?php echo $authImg['sizes']['medium']; ?>"/>
	 			</div>
	 		<?php } ?>
		</div>
		<div class="post_info_container roster">
			<?php 
				$theID = get_the_id();
				$authTitle = get_field('title_at_outdustry', 'user_'.$auth->ID);
				?>
			<div class="post_info">
				<?php if(!empty($authTitle)){ ?>
					<h5><?php echo $authTitle; ?></h5>
				<?php } ?>
				<h1 class="entry-title">
					<?php echo $auth->display_name; ?>
				</h1>
				<?php $headerText = get_field('header_text', 'user_'.$auth->ID); ?>
				<?php $excerpt = get_field('author_excerpt', 'user_'.$auth->ID); ?>
				<?php $authdesc = get_the_author_meta('description', $auth->ID); ?>
				<?php if(!empty($headerText)){ ?>
					<div class="skills_list"><?php echo $headerText; ?></div>
				<?php } ?>
				
			</div>
			<section class="entry-content">
				<?php if(!empty($excerpt)){ 
					echo '<p>'.$excerpt.'</p>';
				 } ?>
				 <?php if(!empty($authdesc) || $authdesc){ 
					echo '<p>'.$authdesc.'</p>';
				 } ?>
			</section>
		</div>
	</div>
</div>

</header>
<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'entry' ); ?>
<?php endwhile; ?>
<?php get_template_part( 'nav', 'below' ); ?>
</section>
<?php get_footer(); ?>