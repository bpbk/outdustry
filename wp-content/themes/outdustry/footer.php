<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
	<div id="logo_small">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
			<img src="<?php echo get_template_directory_uri(); ?>/images/outdustry_logo_small.png"/>
		</a>
	</div>
	<h6><a class="brackets" href="mailto:<?php echo get_option('admin_email'); ?>"><?php echo get_option('admin_email'); ?></a></h6>
</footer>
</div>
<?php wp_footer(); ?>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
</body>
</html>