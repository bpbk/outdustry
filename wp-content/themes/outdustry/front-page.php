<?php get_header(); ?>
<section id="content" role="main">
	<h5 class="section_header header_spaced header_upper"><a href="<?php echo get_the_permalink(7); ?>"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('title_projects', 'option'), false); ?></a></h5>
	<div id="all_projects">
		<div class="projects_gutter"></div>
		<?php $args = array('post_type'=>'project'); ?>
		<?php echo get_the_isotope_posts($args, 'project'); ?>
	</div>
	<div id="mobile_home" class="mobile">
		<div id="search" class="mobile">
			<?php get_search_form(); ?>
		</div>
		<div class="home_content">
			<div class="home_text">
				<?php the_content(); ?>
			</div>
			<h4 class="home_email"><a class="brackets" href="mailto:<?php echo get_option('admin_email'); ?>"><?php echo get_option('admin_email'); ?></a></h4>
			<div class="outdustry_social">
				<?php if(get_field('instagram_link', 'option')){ ?>
					<div id="social_item_insta" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_insta_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('instagram_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('facebook_link', 'option')){ ?>
					<div id="social_item_fb" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_fb_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('facebook_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('linkedIn_link', 'option')){ ?>
					<div id="social_item_li" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_li_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('linkedIn_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('twitter_link', 'option')){ ?>
					<div id="social_item_twitter" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_twitter_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('twitter_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('weibo_link', 'option')){ ?>
					<div id="social_item_weibo" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_weibo_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('weibo_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<script>
	$(document).ready(function(){
		$('#container').imagesLoaded( function() {
			$('#all_projects').isotope({
				itemSelector: 'article',
				layoutMode: 'packery',
				packery: {
					gutter: '.projects_gutter'
				}
			});
		});
	});
</script>
<?php get_footer(); ?>