<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
	load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small', 200 );
	add_image_size( 'productthumb', 500 );
	add_image_size( 'medium', 800 );
	add_image_size( 'large', 1400 );	
	add_image_size( 'full', 1900 );
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = 640;
		register_nav_menus( array( 'main-menu' => __( 'Main Menu', 'blankslate' ) ) );
}

add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

add_filter( 'body_class', function( $classes ) {
    if ( !wp_is_mobile() ) {
        array_push($classes, 'is_desktop');
    }else{
	    array_push($classes, 'is_mobile');
    }
    return $classes;
} );

add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
	wp_enqueue_script( 'jquery' );
	
	wp_register_script( 'imagesloaded', get_template_directory_uri()."/scripts/imagesloaded.pkgd.min.js");
	wp_enqueue_script("imagesloaded");
	
	wp_register_script( 'scripts', get_template_directory_uri()."/scripts/scripts.js");
	wp_enqueue_script("scripts");
	
	wp_register_script( 'jquery-easing', "https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js");
	wp_enqueue_script("jquery-easing");
	
	wp_register_script( 'masonry', get_template_directory_uri()."/scripts/masonry.pkgd.min.js");
	wp_enqueue_script("masonry");
	
	wp_register_script( 'isotope', get_template_directory_uri()."/scripts/isotope.pkgd.min.js");
	wp_enqueue_script("isotope");
	
	wp_register_script( 'packery', get_template_directory_uri()."/scripts/packery-mode.pkgd.min.js");
	wp_enqueue_script("packery");

	wp_register_script( 'cookie', get_template_directory_uri()."/scripts/js.cookie.js");
	wp_enqueue_script("cookie");
	
	wp_register_script( 'slick', get_template_directory_uri()."/scripts/slick/slick.js");
	wp_enqueue_script("slick");
	
	wp_enqueue_style( 'jquery-ui-style',"https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css");

	wp_register_script( 'jquery-ui', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js");
	wp_enqueue_script("jquery-ui");
	
	wp_enqueue_style( 'slick-style',get_template_directory_uri()."/scripts/slick/slick.css");
	
}

add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
	if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
	if ( $title == '' ) {
		return '&rarr;';
	} else {
		return $title;
	}
}

add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
	return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
	register_sidebar( array (
		'name' => __( 'Sidebar Widget Area', 'blankslate' ),
		'id' => 'primary-widget-area',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => "</li>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}

function blankslate_custom_pings( $comment )
{
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}

add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
	if ( !is_admin() ) {
		global $id;
		$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
		return count( $comments_by_type['comment'] );
	} else {
		return $count;
	}
}

/*--------------------------------*\
	PROJECTS POST TYPE
\*--------------------------------*/

add_action('init', 'add_custom_posttypes');
function add_custom_posttypes() {
 
	$labels = array(
		'name' => _x('Projects', 'post type general name'),
		'singular_name' => _x('Project', 'post type singular name'),
		'add_new' => _x('Add New', 'Project'),
		'add_new_item' => __('Add New Project'),
		'edit_item' => __('Edit Project'),
		'new_item' => __('New Project'),
		'view_item' => __('View Project'),
		'search_items' => __('Search Project'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail'),
		'taxonomies' => array('category', 'country', 'service', 'format', 'project_type'),
	  ); 
 
	register_post_type( 'project' , $args );
	
	$labels = array(
		'name' => _x('Roster', 'post type general name'),
		'singular_name' => _x('Roster Member', 'post type singular name'),
		'add_new' => _x('Add New', 'Roster'),
		'add_new_item' => __('Add New Roster Member'),
		'edit_item' => __('Edit Roster Member'),
		'new_item' => __('New Roster Member'),
		'view_item' => __('View Roster Member'),
		'search_items' => __('Search Roster Members'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_template_directory_uri() . '/images/icons/roster.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'show_in_nav_menus' => true,
		'has_archive' => true,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail'),
		'taxonomies' => array('category', 'skills', 'country'),
	  ); 
 
	register_post_type( 'roster' , $args );
	
	$labels = array(
		'name' => _x('Team', 'post type general name'),
		'singular_name' => _x('Team Member', 'post type singular name'),
		'add_new' => _x('Add New', 'Team'),
		'add_new_item' => __('Add New Team Member'),
		'edit_item' => __('Edit Team Member'),
		'new_item' => __('New Team Member'),
		'view_item' => __('View Team Member'),
		'search_items' => __('Search Team Members'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_template_directory_uri() . '/images/icons/team.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail'),
		'taxonomies' => array('category'),
	  ); 
 
	register_post_type( 'team' , $args );
}

/*--------------------------------*\
	CUSTOM TAXONOMIES
\*--------------------------------*/

add_action( 'init', 'custom_taxonomy_Items' );

// Register Custom Taxonomy
function custom_taxonomy_Items()  {

$labels = array(
    'name'                       => 'Country',
    'singular_name'              => 'Country',
    'menu_name'                  => 'Countries',
    'all_items'                  => 'All Countries',
    'parent_item'                => 'Parent Country',
    'parent_item_colon'          => 'Parent Country:',
    'new_item_name'              => 'New Country Name',
    'add_new_item'               => 'Add New Country',
    'edit_item'                  => 'Edit Country',
    'update_item'                => 'Update Country',
    'separate_items_with_commas' => 'Separate Countries with commas',
    'search_items'               => 'Search Countries',
    'add_or_remove_items'        => 'Add or remove Countries',
    'choose_from_most_used'      => 'Choose from the most used Countries',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'country', array('project', 'post', 'roster'), $args );
register_taxonomy_for_object_type( 'country', array('project', 'post', 'roster') );



$labels = array(
    'name'                       => 'Service',
    'singular_name'              => 'Service',
    'menu_name'                  => 'Services',
    'all_items'                  => 'All Services',
    'parent_item'                => 'Parent Service',
    'parent_item_colon'          => 'Parent Service:',
    'new_item_name'              => 'New Service Name',
    'add_new_item'               => 'Add New Service',
    'edit_item'                  => 'Edit Service',
    'update_item'                => 'Update Service',
    'separate_items_with_commas' => 'Separate Services with commas',
    'search_items'               => 'Search Services',
    'add_or_remove_items'        => 'Add or remove Services',
    'choose_from_most_used'      => 'Choose from the most used Services',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => false,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'service', array('project', 'post'), $args );
register_taxonomy_for_object_type( 'service', array('project', 'post') );

$labels = array(
    'name'                       => 'Format',
    'singular_name'              => 'Format',
    'menu_name'                  => 'Formats',
    'all_items'                  => 'All Formats',
    'parent_item'                => 'Parent Format',
    'parent_item_colon'          => 'Parent Format:',
    'new_item_name'              => 'New Format Name',
    'add_new_item'               => 'Add New Format',
    'edit_item'                  => 'Edit Format',
    'update_item'                => 'Update Format',
    'separate_items_with_commas' => 'Separate Formats with commas',
    'search_items'               => 'Search Formats',
    'add_or_remove_items'        => 'Add or remove Formats',
    'choose_from_most_used'      => 'Choose from the most used Formats',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => false,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'format', array('project'), $args );
register_taxonomy_for_object_type( 'format', array('project') );

$labels = array(
    'name'                       => 'Project Type',
    'singular_name'              => 'Project Type',
    'menu_name'                  => 'Project Types',
    'all_items'                  => 'All Project Types',
    'parent_item'                => 'Parent Project Type',
    'parent_item_colon'          => 'Parent Project Type:',
    'new_item_name'              => 'New Project Type Name',
    'add_new_item'               => 'Add New Project Type',
    'edit_item'                  => 'Edit Project Type',
    'update_item'                => 'Update Project Type',
    'separate_items_with_commas' => 'Separate Project Types with commas',
    'search_items'               => 'Search Project Types',
    'add_or_remove_items'        => 'Add or remove Project Types',
    'choose_from_most_used'      => 'Choose from the most used Project Types',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => false,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'project_type', array('project'), $args );
register_taxonomy_for_object_type( 'project_type', array('project') );

$labels = array(
    'name'                       => 'Topic',
    'singular_name'              => 'Topic',
    'menu_name'                  => 'Topics',
    'all_items'                  => 'All Topics',
    'parent_item'                => 'Parent Topic',
    'parent_item_colon'          => 'Parent Topic:',
    'new_item_name'              => 'New Topic Name',
    'add_new_item'               => 'Add New Topic',
    'edit_item'                  => 'Edit Topic',
    'update_item'                => 'Update Topic',
    'separate_items_with_commas' => 'Separate Topics with commas',
    'search_items'               => 'Search Topics',
    'add_or_remove_items'        => 'Add or remove Topics',
    'choose_from_most_used'      => 'Choose from the most used Topics',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => false,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'topic', array('post'), $args );
register_taxonomy_for_object_type( 'topic', array('post') );

/*$labels = array(
    'name'                       => 'Roles',
    'singular_name'              => 'Role',
    'menu_name'                  => 'Roles',
    'all_items'                  => 'All Roles',
    'parent_item'                => 'Parent Role',
    'parent_item_colon'          => 'Parent Role:',
    'new_item_name'              => 'New Role Name',
    'add_new_item'               => 'Add New Role',
    'edit_item'                  => 'Edit Role',
    'update_item'                => 'Update Role',
    'separate_items_with_commas' => 'Separate Roles with commas',
    'search_items'               => 'Search Roles',
    'add_or_remove_items'        => 'Add or remove roles',
    'choose_from_most_used'      => 'Choose from the most used roles',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => false,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'roles', array('roster'), $args );
register_taxonomy_for_object_type( 'roles', array('roster') );*/

$labels = array(
    'name'                       => 'Skills',
    'singular_name'              => 'Skill',
    'menu_name'                  => 'Skills',
    'all_items'                  => 'All Skills',
    'parent_item'                => 'Parent Skill',
    'parent_item_colon'          => 'Parent Skill:',
    'new_item_name'              => 'New Skill Name',
    'add_new_item'               => 'Add New Skill',
    'edit_item'                  => 'Edit Skill',
    'update_item'                => 'Update Skill',
    'separate_items_with_commas' => 'Separate Skills with commas',
    'search_items'               => 'Search Skills',
    'add_or_remove_items'        => 'Add or remove skills',
    'choose_from_most_used'      => 'Choose from the most used skills',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => false,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'skills', array('roster'), $args );
register_taxonomy_for_object_type( 'skills', array('roster') );

}

function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	}	
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}
 
function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
    	array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	}	
	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content); 
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

function setup_theme_admin_menus() {  
    add_submenu_page('options-general.php',  
        'Outdustry Settings', 'Outdustry Settings', 'manage_options',  
        'outdustry-elements', 'outdustry_settings');     
}  

function outdustry_settings() {  
 if (!current_user_can('manage_options')) {  
    wp_die('You do not have sufficient permissions to access this page.');  
}      
if (isset($_POST["update_settings"])) {  


$instaLink = esc_attr($_POST["instaLink"]);  
update_option("instaLink", $instaLink);
$fbLink = esc_attr($_POST["fbLink"]);  
update_option("fbLink", $fbLink);
$liLink = esc_attr($_POST["liLink"]);  
update_option("liLink", $liLink); 
$twitterLink = esc_attr($_POST["twitterLink"]);  
update_option("twitterLink", $twitterLink);
$weiboLink = esc_attr($_POST["weiboLink"]);  
update_option("weiboLink", $weiboLink);
$weChatLink = esc_attr($_POST["weChatLink"]);  
update_option("weChatLink", $weChatLink);


?>  
<div id="message" class="updated">Settings saved</div>  
<?php }   

?>
 <div class="wrap">  
        <?php screen_icon('themes'); ?> <h2>Settings</h2>  
        <form method="POST" action="">  
            <table class="form-table">  
                <tr valign="top">  
                    <th scope="row">  
                        <label for="instaLink">  
                          	Instagram Link:
                        </label>  
                    </th>  
                    <td>  
                        <input size="50" id="instaLink" name="instaLink" value="<?php echo get_option("instaLink"); ?>" type="text">
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">  
                        <label for="fbLink">  
                           Facebook Link:
                        </label>  
                    </th>  
                    <td>  
                        <input size="50" id="fbLink" name="fbLink" value="<?php echo get_option("fbLink"); ?>" type="text">
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">  
                        <label for="liLink">  
                           LinkedIn Link:
                        </label>  
                    </th>  
                    <td>  
                        <input size="50" id="liLink" name="liLink" value="<?php echo get_option("liLink"); ?>" type="text">
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">  
                        <label for="twitterLink">  
                           Twitter Link:
                        </label>  
                    </th>  
                    <td>  
                        <input size="50" id="twitterLink" name="twitterLink" value="<?php echo get_option("twitterLink"); ?>" type="text">
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">  
                        <label for="weiboLink">  
                           Weibo Link:
                        </label>  
                    </th>  
                    <td>  
                        <input size="50" id="weiboLink" name="weiboLink" value="<?php echo get_option("weiboLink"); ?>" type="text">
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">  
                        <label for="weiboLink">  
                           WeChat Link:
                        </label>  
                    </th>  
                    <td>  
                        <input size="50" id="weChatLink" name="weChatLink" value="<?php echo get_option("weChatLink"); ?>" type="text">
                    </td>  
                </tr>
            </table>  
            <p>  
    <input type="submit" value="Save settings" class="button-primary"/>  
</p>  
            <input type="hidden" name="update_settings" value="Y" /> 
        </form>  
    </div>  
    <?php
}          

add_action("admin_menu", "setup_theme_admin_menus"); 
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'roster_to_projects',
        'from' => 'roster',
        'to' => 'project'
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );

add_action('wp_head','pluginname_ajaxurl');

function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}


add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
     switch($_REQUEST['fn']){
        case 'next_posts':
        	$paged = $_REQUEST['paged'];
        	$posttype = $_REQUEST['posttype'];
        	$countries = json_decode(stripslashes($_REQUEST['country']));
        	$formats = json_decode(stripslashes($_REQUEST['format']));
        	$services = json_decode(stripslashes($_REQUEST['service']));
        	$pTypes = json_decode(stripslashes($_REQUEST['ptype']));
        	$skills = json_decode(stripslashes($_REQUEST['skills']));
        	$roster = json_decode(stripslashes($_REQUEST['rosters']));
        	$topics = json_decode(stripslashes($_REQUEST['topic']));
        	$args = array('post_type' => $posttype, 'posts_per_page' => get_option( 'posts_per_page' ));
		    if($paged && $paged != ''){
				$args['paged'] = $paged;
			}
        	$output = get_the_isotope_posts($args, $posttype, $countries, $formats, $services, $pTypes, $skills, $roster, $topics);
            echo $output;
        break;
        default:
            $output = 'No function specified, check your jQuery.ajax() call';
        break;
     }
     die;
}

function pre_isotope_posts($q, $pt){
		$countries = false;
		if(isset($_GET['country'])){
			$countries = explode(',', $_GET['country']);
		}
		$formats = false;
	    if(isset($_GET['format'])){
		    $formats = explode(',', $_GET['format']);
	    }
	    $services = false;
	    if(isset($_GET['service'])){
		    $services = explode(',', $_GET['service']);
	    }
	    $pTypes = false;
	    if(isset($_GET['project_type'])){
		    $pTypes = explode(',', $_GET['project_type']);
	    }
	    $skills = false;
	    if(isset($_GET['skills'])){
		    $skills = explode(',', $_GET['skills']);
	    }
	    $topics = false;
	    if(isset($_GET['topic'])){
			$topics = explode(',', $_GET['topic']);
	    }
	    $rosters = false;
	    if(isset($_GET['rosters'])){
			$rosters = explode(',', $_GET['rosters']);
	    }
	    $args = array('post_type' => $pt, 'posts_per_page' => get_option( 'posts_per_page' ));
	    if($q && $q != ''){
			$args['paged'] = $q;
		}
	    
	    return get_the_isotope_posts($args, $pt, $countries, $formats, $services, $pTypes, $skills, $rosters, $topics );
}

function get_the_isotope_posts($args, $posttype = 'project', $countries = false, $formats = false, $services = false, $pTypes = false, $skills = false, $roster = false, $topics = false){
    
    global $post;
    
	if(!empty($countries) || !empty($formats) || !empty($services) || !empty($pTypes) || !empty($skills) || !empty($roster) || !empty($topics)){
		$args['tax_query'] = array('relation' => 'OR');
	}
	if(!empty($countries)){
		array_push($args['tax_query'], array('taxonomy' => 'country', 'field' => 'slug', 'terms' => $countries));
	}
	if(!empty($formats)){
		array_push($args['tax_query'], array('taxonomy' => 'format', 'field' => 'slug', 'terms' => $formats));
	}
	if(!empty($services)){
		array_push($args['tax_query'], array('taxonomy' => 'service', 'field' => 'slug', 'terms' => $services));
	}
	if(!empty($pTypes)){
		array_push($args['tax_query'], array('taxonomy' => 'project_type', 'field' => 'slug', 'terms' => $pTypes));
	}
	if(!empty($skills)){
		array_push($args['tax_query'], array('taxonomy' => 'skills', 'field' => 'slug', 'terms' => $skills));
	}
	if(!empty($roster)){
		$rosterIds = array();
		foreach($roster as $rItem){
			$rosterPost = get_page_by_path( $rItem, OBJECT, 'roster' );
			$id = $rosterPost->ID;
			array_push($rosterIds, $id);
		}
		$args['connected_type'] = 'roster_to_projects';
		$args['connected_items'] = $rosterIds;
	}
	if(!empty($topics)){
		array_push($args['tax_query'], array('taxonomy' => 'topic', 'field' => 'slug', 'terms' => $topics));
	}
	$projects_query = new WP_Query( $args ); 
	if ( $projects_query->have_posts() ) :
		ob_start();
		while ( $projects_query->have_posts() ) : $projects_query->the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="archive_post_container">
				<?php if($posttype == 'roster' || $posttype == 'post' || $posttype == 'search'){ ?>
					<div class="archive_post_image archive_post_image_holder">
						<div class="archive_post_image_bg  bg_centered" style="background-image:url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>);">
							<a href="<?php echo get_the_permalink(); ?>"></a>
						</div>
					</div>
				<?php }else{ ?>
					<div class="archive_post_image">
						<img src="<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>"/>
						<a href="<?php echo get_the_permalink(); ?>"></a>
					</div>
				<?php } ?>
				<div class="archive_post_content_container">
					<div class="archive_post_content">
						<div class="archive_posts_content_inner">
							<?php if($posttype == 'project'){ ?>
							<?php $term = get_the_terms(get_the_id(), 'format'); ?>
							<?php if(count($term) > 0){ ?>
								<?php $theFormat = get_taxonomy('format'); ?>
								<h6 class="project_format"><span><?php echo $theFormat->label; ?>: <a href="<?php echo get_home_url().'/projects/?format='.$term[0]->slug; ?>"><?php echo $term[0]->name; ?></a></span></h6>
							<?php } ?>
							<?php }else if($posttype == 'post'){ ?>
								<h5 class="entry-date"><span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span></h5>
							<?php }else if($posttype == 'search'){ ?>
								<h6 style="font-size:14px;" class="project_format"><span><?php echo get_post_type(); ?></span></h6>
							<?php } ?>
							<h4 class="entry-title <?php echo $posttype; ?>-title">
								<span>
									<a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a>
								</span>
							</h4>
							<?php if(get_field('project_artist') && $posttype != 'search'){ ?>
								<h4 class="artist_header"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('the_artist', 'option'), false); ?>: <a href="<?php echo get_site_url() ?>?s=<?php echo get_field('project_artist'); ?>"><?php echo get_field('project_artist'); ?></a></h4>
							<?php } ?>
							<?php if($posttype == 'post'){ ?>
								<div class="archive_post_excerpt">
									<?php echo excerpt(25); ?>
								</div>
							<?php } ?>
							<?php $bracketTaxes = get_the_terms(get_the_id(), 'service'); ?>
							<?php $bracketTaxType = 'service'; ?>
							<?php if($posttype == 'roster'){ ?>
								<?php $bracketTaxes = get_the_terms(get_the_id(), 'skills'); ?>
								<?php $bracketTaxType = 'skills'; ?>
								<?php if(get_field('title')){ ?>
									<h4 class="roster_sub_header"><span><?php echo get_field('title'); ?></span></h4>
								<?php } ?>
							<?php } ?>
							<?php $filterPage = 'projects'; ?>
							<?php if(is_home()){ ?>
								<?php $filterPage = 'blog'; ?>
							<?php }else if(get_post_type() == 'roster'){ ?>
								<?php $filterPage = 'roster'; ?>
							<?php } ?>
							<?php if($bracketTaxes && count($bracketTaxes) > 0 && $posttype != 'search'){ ?>
								<div class="link_list">
									<?php foreach($bracketTaxes as $bracketTax){ ?>
										<a href="<?php echo get_home_url().'/'.$filterPage.'/?'.$bracketTax->taxonomy.'='.$bracketTax->slug; ?>"><h6 class="brackets main_page_title"><?php echo $bracketTax->name; ?></h6></a>
									<?php } ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</article>
	<?php endwhile;
		wp_reset_postdata();
		return ob_get_clean();
	else:
		return false;
	endif;
}

function get_the_isotope_post_count($args){
	$projects_query = new WP_Query( $args ); 
	if ( $projects_query->have_posts() ) :
		return $projects_query->post_count;
	else:
		return false;
	endif;
	
	wp_reset_postdata();
}

function get_related_content($args){
	$content_query = new WP_Query( $args ); 
	if ( $content_query->have_posts() ) :
		ob_start();
		while ( $content_query->have_posts() ) : $content_query->the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="related_content_container">
				<div class="related_content_image bg_centered" style="background-image:url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>)">
					<a href="<?php echo get_the_permalink(); ?>"></a>
				</div>
				<div class="related_content">
					<h3 class="entry-title related-content-title">
						<span>
							<a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a>
						</span>
					</h3>
				</div>
			</div>
		</article>
	<?php endwhile;
		wp_reset_postdata();
		return ob_get_clean();
	else:
		return false;
	endif;
}

/*function get_roster_posts($paged = false){
	$args = array('post_type' => 'roster', 'posts_per_page' => 4);
	if($paged && $paged != ''){
		$args['paged'] = $paged;
	}
	$roster_query = new WP_Query( $args ); 
	if ( $roster_query->have_posts() ) : ?>
		<?php while ( $roster_query->have_posts() ) : $roster_query->the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="archive_post_container">
					<div class="archive_post_image bg_centered" style="background-image:url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>)">
						<a href="<?php echo get_the_permalink(); ?>"></a>
					</div>
					<div class="archive_post_content_container">
						<div class="archive_post_content">
							<div class="archive_posts=_content_inner">
								<h3 class="entry-title roster-title">
									<span>
										<a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a>
									</span>
								</h3>
								<?php $format = get_the_terms(get_the_id(), 'format'); ?>
								<?php if(count($format) > 0){ ?>
									<h4 class="roster_sub_header"><span>Lorem Ipsum</span></h4>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</article>
		<?php endwhile; ?>
	<?php wp_reset_postdata();
		endif;
}*/