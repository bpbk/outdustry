<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
<div id="header_main">
	<div id="header_main_content_left">
		<?php if(!is_front_page()){ ?>
			<div id="logo_small">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
					<img src="<?php echo get_template_directory_uri(); ?>/images/outdustry_logo_small.png"/>
				</a>
			</div>
		<?php } ?>
		<nav id="menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
		</nav>
		<div class="clear"></div>
	</div>
	<div id="header_main_content_right">
		<div id="site-description"><?php bloginfo( 'description' ); ?></div>
		<div id="language_switch" class="header_button"><?php echo qtranxf_generateLanguageSelectCode('both'); ?></div>
		<div id="search_button" class="header_button"></div>
		<div id="social_button" class="header_button"></div>
		<div id="contact_button" class="header_button"></div>
	</div>
</div>
<?php if(is_front_page()){ ?>
	<section id="logo_big">
		<div id="site-title">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
				<img src="<?php echo get_template_directory_uri(); ?>/images/outdustry_logo.png"/>
			</a>
		</div>
	</section>
<?php } ?>
<?php if(is_home()){ ?>
	<?php $blogPage = get_page(get_option('page_for_posts')); ?>
	<h1 class="blog_header_title"><?php echo qtranxf_use(qtrans_getLanguage(), $blogPage->post_title,false); ?></h1>
<?php } ?>
<div id="header_right">
	<div class="mobile logo_white">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
			<img src="<?php echo get_template_directory_uri(); ?>/images/outdustry_logo_white.png"/>
		</a>
	</div>
	<div id="search" class="mobile">
		<?php get_search_form(); ?>
	</div>
	<nav id="menu" role="navigation" class="mobile">
		<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
	</nav>
	<div class="outdustry_social">
		<?php if(get_field('instagram_link', 'option')){ ?>
			<div id="social_item_insta" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_insta_black.png'; ?>);">
				<a target="_blank" href="<?php echo get_field('instagram_link', 'option'); ?>"></a>
			</div>
		<?php } ?>
		<?php if(get_field('facebook_link', 'option')){ ?>
			<div id="social_item_fb" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_fb_black.png'; ?>);">
				<a target="_blank" href="<?php echo get_field('facebook_link', 'option'); ?>"></a>
			</div>
		<?php } ?>
		<?php if(get_field('linkedIn_link', 'option')){ ?>
			<div id="social_item_li" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_li_black.png'; ?>);">
				<a target="_blank" href="<?php echo get_field('linkedIn_link', 'option'); ?>"></a>
			</div>
		<?php } ?>
		<?php if(get_field('twitter_link', 'option')){ ?>
			<div id="social_item_twitter" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_twitter_black.png'; ?>);">
				<a target="_blank" href="<?php echo get_field('twitter_link', 'option'); ?>"></a>
			</div>
		<?php } ?>
		<?php if(get_field('weibo_link', 'option')){ ?>
			<div id="social_item_weibo" class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_weibo_black.png'; ?>);">
				<a target="_blank" href="<?php echo get_field('weibo_link', 'option'); ?>"></a>
			</div>
		<?php } ?>
	</div>
	<div id="header_mailing_list" class="mailing_list_link">
		<div id="header_mailing_list_text">
			<?php $mlPage = get_page(80); ?>
			<?php $mlTitle = qtranxf_use(qtrans_getLanguage(), $mlPage->post_title,false); ?>
			<?php $mlContent = qtranxf_use(qtrans_getLanguage(), $mlPage->post_content,false); ?>
			<?php $mlExcerpt = qtranxf_use(qtrans_getLanguage(), $mlPage->post_excerpt,false); ?>
			<h5 class="header_spaced"><?php echo $mlTitle; ?></h5>
			<p><?php echo $mlContent; ?></p>
		</div>
		<div id="header_mailing_list_arrow">
			
		</div>
	</div>
	<?php if(is_front_page()){ ?>
		<?php $blogPage = get_page(get_option('page_for_posts')); ?>
		<section id="home_sidebar">
			<h5 class="section_header header_spaced header_upper"><a href="<?php echo get_the_permalink(get_option('page_for_posts')); ?>"><?php echo qtranxf_use(qtrans_getLanguage(), $blogPage->post_title,false); ?></a></h5>
			<?php
			$args = array('post_type' => 'post', 'posts_per_page' => 5);
			$post_query = new WP_Query( $args ); 
			if ( $post_query->have_posts() ) : ?>
			<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="archive_post_container">
						<div class="archive_post_image">
							<div class="archive_post_image_bg bg_centered" style="background-image:url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>)">
								<a href="<?php echo get_the_permalink(); ?>"></a>
							</div>
						</div>
						<div class="archive_post_content_container">
							<div class="archive_post_content">
								<div class="archive_posts_content_inner">
									<?php $topics = get_the_terms(get_the_id(), 'topic'); ?>
									<?php if($post_query->current_post == 0){ ?>
										<h5 class="header_spaced blog_type"><?php echo $blogPage->post_title; ?>: FEATURE</h5>
									<?php }else if(count($topics) > 0 && $topics && $post_query->current_post != 0){ ?>
										<h6 class="header_spaced blog_type"><?php echo $blogPage->post_title; ?>: <?php echo $topics[0]->name; ?></h6>
									<?php } ?>
										
									<<?php echo $post_query->current_post == 0 ? 'h3' : 'h4'; ?> class="entry-title">
										<a href="<?php echo get_the_permalink(); ?>"><span><?php the_title(); ?></span></a>
									</<?php echo $post_query->current_post == 0 ? 'h3' : 'h4'; ?>>
									<h6 class="entry-date"><span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span></h6>
								</div>
							</div>
						</div>
					</div>
				</article>
			<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</section>
	<?php } ?>
</div>

<div class="clear"></div>
	<div id="contact_form" class="overlay_container">
		<div class="overlay_background">
		</div>
		<div class="overlay_content">
			<div class="overlay_content_inner">
				<h4 class="overlay_header header_spaced contact_us_header header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('contact_popup_header', 'option'), false); ?></h4>
				<h4 class="overlay_header header_spaced find_us_header header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('social_popup_header', 'option'), false); ?></h4>
				<div class="contact_email">
					<a href="mailto:<?php echo get_option('admin_email'); ?>"><?php echo get_option('admin_email'); ?></a>
				</div>
				<div id="share_buttons">
					<?php if(get_option('fbLink')){ ?>
						<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_fb.png'; ?>);">
							<a href="<?php echo get_option('fbLink'); ?>" title="Our Facebook."></a>
						</div>
					<?php } ?>
					<?php if(get_option('twitterLink')){ ?>
						<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_twitter.png'; ?>);">
							<a target="_blank" rel="nofollow" href="<?php echo get_option('twitterLink'); ?>" title="Our Twitter."></a>
						</div>
					<?php } ?>
					<?php if(get_option('liLink')){ ?>
						<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_li.png'; ?>);">
							<a href="<?php echo get_option('liLink'); ?>" title="Our LinkedIn."></a>
						</div>
					<?php } ?>
					<?php if(get_option('weChatLink')){ ?>
						<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_wechat.png'; ?>);">
							<a href="<?php echo get_option('weChatLink'); ?>" title="Our WeChat."></a>
						</div>
					<?php } ?>
					<?php if(get_option('weiboLink')){ ?>
						<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_weibo.png'; ?>);">
							<a href="<?php echo get_option('weiboLink'); ?>" title="Our Weibo."></a>
						</div>
					<?php } ?>
				</div>
				<?php echo do_shortcode('[contact-form-7 id="77" title="Contact Us"]'); ?>
				<h5 class="close_overlay header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('close_overlay', 'option'), false); ?></h5>
			</div>
		</div>
	</div>
	
	<div id="search_overlay" class="overlay_container">
		<div class="overlay_background">
		</div>
		<div class="overlay_content">
			<div class="overlay_content_inner">
				<h4 class="overlay_header header_spaced header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('search_popup_header', 'option'), false); ?></h4>
				<div id="search">
					<?php get_search_form(); ?>
				</div>
				<h5 class="close_overlay header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('close_overlay', 'option'), false); ?></h5>
			</div>
		</div>
	</div>
				
	<div id="mailing_list_form" class="overlay_container">
		<div class="overlay_background">
		</div>
		<div class="overlay_content">
			<div class="overlay_content_inner">
				<h4 class="overlay_header header_spaced header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('newsletter_popup_header', 'option'), false); ?></h4>
				<div class="mailing_form_excerpt">
					<p><?php echo $mlExcerpt; ?></p>
				</div>
				<div class="mailing_list_form_response response_already_signed_up">
					<?php echo qtranxf_use(qtrans_getLanguage(), get_field('newsletter_already_signed_up', 'option'), false); ?>
				</div>
				<div class="mailing_list_form_response response_failed">
					<?php echo qtranxf_use(qtrans_getLanguage(), get_field('newsletter_invalid', 'option'), false); ?>
				</div>
				<div class="mailing_list_form_holder">
					<input class="mailing_list_input" placeholder="email@email.com">
					<input type="submit" class="submit" value="sign me up">
				</div>
				<div class="mailing_list_form_response response_signed_up">
					<?php echo qtranxf_use(qtrans_getLanguage(), get_field('thanks_for_signing_up', 'option'), false); ?>
				</div>
				<h5 class="close_overlay header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('close_overlay', 'option'), false); ?></h5>
			</div>
		</div>
	</div>
	
<?php if(is_single() && get_post_type() == 'post'){ ?>
	<div id="share_blog" class="overlay_container">
		<?php $blog_link = htmlentities( get_the_permalink() ); ?>
		<?php $blog_title = htmlentities( get_the_title() ); ?>
		<?php if ( has_post_thumbnail() ) { ?>
			<?php $blog_image = htmlentities(  wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ) ); ?>
		<?php } ?>
		<div class="overlay_background"></div>
		<div class="overlay_content">
			<div class="overlay_content_inner">
				<h4 class="overlay_header header_spaced header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('share_popup_header', 'option'), false); ?></h4>
				<div id="share_buttons">
					<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_fb.png'; ?>);">
						<a href="http://www.facebook.com/sharer.php?u=<?php echo $blog_link;?>&amp;t=<?php echo $blog_title; ?>" onclick="javascript:void window.open('http://www.facebook.com/sharer.php?u=<?php echo $blog_link; ?>&amp;t=<?php echo $blog_title; ?>','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Share on Facebook.">
						</a>
					</div>
					<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_twitter.png'; ?>);">
						<a target="_blank" rel="nofollow" href="<?php echo $blog_link; ?>" onclick="javascript:void window.open('https://twitter.com/share?status=<?php echo $blog_link; ?>&text=<?php echo $blog_title; ?>','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Tweet this!">
						</a>
					</div>
					<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_li.png'; ?>);">
						<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $blog_link;?>&amp;title=<?php the_title(); ?>" onclick="javascript:void window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $blog_link; ?>&amp;title=<?php echo $blog_title; ?>','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Share on LinkedIn.">
						</a>
					</div>
					<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_weibo.png'; ?>);">
						<a href="http://v.t.sina.com.cn/share/share.php?url=<?php echo $blog_link;?>&amp;title=<?php echo $blog_title; ?>&pic=<?php echo $blog_image; ?>&appkey=&ralateUid=&language=zh_cn&searchPic=false" onclick="javascript:void window.open('http://v.t.sina.com.cn/share/share.php?url=<?php echo $blog_link;?>&amp;title=<?php echo $blog_title; ?>&pic=<?php echo $blog_image; ?>&appkey=&ralateUid=&language=zh_cn&searchPic=false','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Share on Weibo.">
						</a>
					</div>
					<div class="share_button" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_reddit.png'; ?>);">
						<a href="http://reddit.com/submit?url=<?php echo $blog_link;?>&amp;title=<?php echo $blog_title; ?>&pic=<?php echo $blog_image; ?>&appkey=&ralateUid=&language=zh_cn&searchPic=false" onclick="javascript:void window.open('http://reddit.com/submit?url=<?php echo $blog_link;?>&amp;title=<?php echo $blog_title; ?>&pic=<?php echo $blog_image; ?>&appkey=&ralateUid=&language=zh_cn&searchPic=false','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Share on Reddit.">
						</a>
					</div>
				</div>
				<h5 class="close_overlay header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('close_overlay', 'option'), false); ?></h5>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="side_text">
		<img src="<?php echo get_template_directory_uri(); ?>/images/sidebar_text.png"/>
	</div>
</header>
<header class="mobile" id="header_mobile">
	<div class="header_mobile_content">
		<div id="logo_small">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
				<img src="<?php echo get_template_directory_uri(); ?>/images/outdustry_logo_small.png"/>
			</a>
		</div>
		<div class="navAndSearch">
			<div id="mobile_search" class="header_button"></div>
			<div id="nav_button" class="mobile">
				<!--<div class="nav_button_line">
				</div>-->
				<h5 class="open_menu"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('menu', 'option'), false); ?></h5>
				<h5 class="close_menu"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('close_menu', 'option'), false); ?></h5>
			</div>
		</div>
	</div>
</header>
<div id="container">