<?php get_header(); ?>
<section id="content" role="main" posttype="post">
	<div id="page_sub_header">
		<div id="page_sub_header_container">
			<!--<div id="blog_header_logo">
				<div id="logo_small">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri(); ?>/images/outdustry_logo_small.png"/>
					</a>
					<?php $blogID = get_option('page_for_posts'); ?>
					<?php $blogPage = get_page($blogID); ?>
					<span class="blog_header"><?php echo qtranxf_use(qtrans_getLanguage(), $blogPage->post_title,false); ?></span>
				</div>
			</div>-->
			<h5 class="header_spaced mobile filter_by header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('filter_by', 'option'), false); ?></h5>
			<div id="filter_boxes">
				<div id="filter_box_topic" class="filter_box">
					<?php $theTerm = get_taxonomy('topic'); ?>
					<div class="filter_box_inner">
						<div class="filter_box_content filter_header" slug="topic">
							<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
							<div class="filter_arrow arrow_small">
								<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
							</div>
						</div>
						<?php $terms = get_terms(array('taxonomy' => 'topic' )); ?>
							<?php if(count($terms) > 0){ ?>
								<div class="filter_list">
									<?php foreach($terms as $term){ ?>
										<div class="filter_item topic_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="topic">
											<div class="filter_item_content">
												<div class="filter_checkbox"></div>
												<p><?php echo $term->name; ?></p>
											</div>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
					</div>
				</div>
				<div id="filter_box_country" class="filter_box">
					<?php $theTerm = get_taxonomy('country'); ?>
					<div class="filter_box_inner">
						<div class="filter_box_content filter_header" slug="country">
							<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
							<div class="filter_arrow arrow_small">
								<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
							</div>
						</div>
						<?php $terms = get_terms(array('taxonomy' => 'country' )); ?>
							<?php if(count($terms) > 0){ ?>
								<div class="filter_list">
									<?php foreach($terms as $term){ ?>
										<div class="filter_item country_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="country">
											<div class="filter_item_content">
												<div class="filter_checkbox"></div>
												<p><?php echo $term->name; ?></p>
											</div>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
					</div>
				</div>
				<div id="filter_box_service" class="filter_box">
					<?php $theTerm = get_taxonomy('service'); ?>
					<div class="filter_box_inner">
						<div class="filter_box_content filter_header" slug="service">
							<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
							<div class="filter_arrow arrow_small">
								<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
							</div>
						</div>
						<?php $terms = get_terms(array('taxonomy' => 'service' )); ?>
							<?php if(count($terms) > 0){ ?>
								<div class="filter_list">
									<?php foreach($terms as $term){ ?>
										<div class="filter_item service_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="service">
											<div class="filter_item_content">
												<div class="filter_checkbox"></div>
												<p><?php echo $term->name; ?></p>
											</div>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="active_filter_items">
	</div>
	<div id="all_projects" class="all_posts">
		<div class="projects_gutter"></div>
		<?php echo pre_isotope_posts(1, 'post'); ?>
		<!--<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="archive_post_container <?php echo $wp_query->current_post == 0 ? 'first_post' : ''; ?>">
					<div class="archive_post_image bg_centered" style="background-image:url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>)">
						<a href="<?php echo get_the_permalink(); ?>"></a>
					</div>
					<div class="archive_post_content_container">
						<div class="archive_post_content">
							<div class="archive_posts_content_inner">
								<h5 class="entry-date"><span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span></h5>
								<<?php echo $wp_query->current_post == 0 ? 'h2' : 'h3'; ?> class="entry-title">
									<a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a>
								</<?php echo $wp_query->current_post == 0 ? 'h2' : 'h3'; ?>>
								<div class="archive_post_excerpt">
									<?php echo excerpt(25); ?>
								</div>
								<?php $posttopics = get_the_terms(get_the_id(), 'topic'); ?>
								<?php if($posttopics && count($posttopics) > 0){ ?>
									<div class="link_list">
										<?php foreach($posttopics as $posttopic){ ?>
											<a href="<?php echo get_term_link($posttopic, 'topic'); ?>"><h6 class="brackets main_page_title"><?php echo $posttopic->name; ?></h6></a>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</article>
		<?php endwhile; endif; ?>-->
	</div>
	<!--<?php if(paginate_links()){ ?>
		<div id="post_paging">
			<div class="post_paging_content">
				<?php echo paginate_links( array('prev_next' => false ) ); ?>
			</div>
		</div>
	<?php } ?>-->
	<div class="clear"></div>
</section>
<?php get_footer(); ?>