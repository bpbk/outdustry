<nav id="nav-below" class="navigation" role="navigation">
	<?php $prevPost = get_previous_post(); ?>
	<?php $prevPostTitle = $prevPost->post_title; ?>
	<?php $prevPostFormat = get_the_terms($prevPost->ID, 'format'); ?>
	<?php if($prevPost){ ?>
		<div class="nav-previous"><a href="<?php echo get_the_permalink($prevPost->ID); ?>"><div class="nav-previous-inner"><h5 class="nav_next">NEXT</h5><h5 style="font-size: 24px;"><?php echo $prevPostTitle; ?></h5><?php echo isset($prevPostFormat) ? '<span> <h6 style="margin-left:10px;" class="brackets main_page_title">'.$prevPostFormat[0]->name.'</h6></span>' : ''; ?></div><div class="prev_arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/next.png"></div></a></div>
	<?php } ?>

<!--<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>' ); ?></div>-->
</nav>