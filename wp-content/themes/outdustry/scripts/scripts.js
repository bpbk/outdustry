var $ = jQuery.noConflict();

var filterArray = [];
var filterSlugArray = [];
var $isotopeContainer;
var currentPage = 1;
var loading = false;

var countryArray = [];
var formatArray = [];
var serviceArray = [];
var pTypeArray = [];
var skillsArray = [];
var rosterArray = [];
var topicArray = [];
getUrlParameter();

var postType;

$(document).ready(function(){
	if(filterArray.length > 0){
		$.each(filterArray, function(i, item){
			if($('.filter_item_'+item.slug).length > 0){
				var filterItemName = $('.filter_item_'+item.slug).attr('name');
				$('.filter_item_'+item.slug).addClass('active');
				$('#active_filter_items').append('<div id="active_item_'+item.slug+'" class="active_item">'+filterItemName+'</div>');
				filterSlugArray.push(item.slug);
				$('#active_item_'+item.slug).click(function(){
					removeSlugFromArray(item.slug, item);
					filter_posts();
				});
			}
		});
		$('#active_filter_items').addClass('active');
	}
	$('.close_overlay').click(function(){
		$('.overlay_container').fadeOut(300);
		setTimeout(function(){
			$('.overlay_container').removeClass('find_us');
		}, 300)
	});
	
	$('.post_info_share').click(function(){
		$('#share_blog').fadeIn();
	});
	
	$('.mailing_list_link').click(function(){
		$('#mailing_list_form').fadeIn();
	});
	
	$('#contact_button').click(function(){
		$('#contact_form').fadeIn();
	});
	
	$('#search_button').click(function(){
		$('#search_overlay').fadeIn();
	});
	
	$('#mobile_search').click(function(){
		$('header#header, #nav_button').toggleClass('active');
		$('.overlay_container').hide();
		$('#search input').trigger('focus');
	});
	
	$('#social_button').click(function(){
		$('#contact_form').addClass('find_us');
		$('#contact_form').fadeIn();
	});
	
	$('.filter_header').click(function(){
		var fSlug = $(this).attr('slug');
		
		if(!$('#filter_box_'+fSlug).hasClass('active')){
			$('.filter_box').removeClass('active');
			$('#filter_box_'+fSlug).addClass('active');
		}else{
			$('.filter_box').removeClass('active');
		}
	});
	
	$('.filter_item').click(function(){
		setTimeout(function(){
			$('.filter_box').removeClass('active');
		}, 200);
		var fItemSlug = $(this).attr('slug');
		var fItemName = $(this).attr('name');
		var fItemTax = $(this).attr('tax');
		var $filterItem = $(this);
		var fItemObj = { 'tax': fItemTax, 'slug': fItemSlug };
		
		//console.log(jQuery.inArray(fItemSlug, filterSlugArray));
		if(jQuery.inArray(fItemSlug, filterSlugArray) == -1 ){
			filterArray.push(fItemObj);
			filterSlugArray.push(fItemSlug);
			
			$filterItem.addClass('active');
			$('#active_filter_items').append('<div id="active_item_'+fItemSlug+'" class="active_item">'+fItemName+'</div>');
			if(!$('#active_filter_items').hasClass('active')){
				$('#active_filter_items').addClass('active');
			}
			$('#active_item_'+fItemSlug).click(function(){
				removeSlugFromArray(fItemSlug, fItemObj);
				filter_posts();
			});
		}else{
			$filterItem.removeClass('active');
			removeSlugFromArray(fItemSlug, fItemObj);
		}
		
		loading = true;
		currentPage = 1;
		filter_posts();
	});
	
	if($('#all_projects').length > 0){
		postType = $('#content').attr('posttype');
		$isotopeContainer = $('#all_projects');
		var isotopeObj = {itemSelector: 'article', layoutMode: 'packery'};
		
		if($('#all_projects').hasClass('all_roster')){ // Switch isotope style for roster
			isotopeObj.layoutMode = 'fitRows';
			isotopeObj.fitRows = {gutter:'.projects_gutter'};
		}else{
			isotopeObj.layoutMode = 'packery';
			isotopeObj.packery = {gutter: '.projects_gutter'};
		}
		$isotopeContainer.isotope(isotopeObj);
	}
	
	var searchPlaceholder = $('input.search-field').attr('placeholder');
	$('input.search-field').on('focus', function(){
		var focusText = $(this).attr('focustext');
		$(this).attr('placeholder', focusText );
	});
	$('input.search-field').on('focusout', function(){
		var focusText = $(this).attr('focustext');
		$(this).attr('placeholder', searchPlaceholder );
	});
	
	$('#nav_button').click(function(){
		$('header#header, #nav_button').toggleClass('active');
		$('.overlay_container').hide();
	});
	
	newsletterFunction();
	
});

function newsletterFunction(){
	
	// Get list info
	/*$.ajax({
		url: templateurl+'/mail/getlist.php',
		data: {},
		dataType: 'json',
		success: function(data) {
			console.log(data);
		},
		complete:function(){
		},
		error: function(errorThrown) {
			console.log(errorThrown);
		}
	});*/
	
	
	
	$('.mailing_list_form_holder .submit').click(function(){
		var nlEmail = $('input.mailing_list_input', $(this).parent()).val();
		if(validateEmail(nlEmail)){
			$('input.mailing_list_input', $(this).parent()).css('border', 'none');
			$.ajax({
				url: templateurl+'/mail/signup.php',
				data: {
					'nlemail':nlEmail
				},
				//dataType: 'json',
				success: function(data) {
					//console.log(data);
					var nlJson = $.parseJSON( data );
					var nlmessage = '';
					//console.log(nlJson);
					$('.mailing_list_form_response').hide();
					if(nlJson.title == 'Member Exists'){
						setTimeout(function(){
							$('.response_already_signed_up').slideDown();
						}, 300);
					}else if(nlJson.status = 'subscribed'){
						$('.mailing_list_form_holder').slideUp();
						setTimeout(function(){
							$('.response_signed_up').slideDown();
						}, 300);
					}else if(nlJson.title == "Invalid Resource"){
						setTimeout(function(){
							$('.response_failed').slideDown();
						}, 300);
					}else{
						setTimeout(function(){
							$('.response_failed').slideDown();
						}, 300);
					}
				},
				complete:function(){
				},
				error: function(errorThrown) {
					console.log(errorThrown);
				}
			});
		}else{
			$('input.mailing_list_input', $(this).parent()).css('border', '2px solid red');
		}
	});
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function removeSlugFromArray(slug, obj){
	$('#active_item_'+slug).remove();
	$('.filter_item_'+slug).removeClass('active');
	filterArray = jQuery.grep(filterArray, function(value) {
		return value.slug != slug;
	});
	filterSlugArray = jQuery.grep(filterSlugArray, function(value) {
		return value != slug;
	});
	if(filterArray.length == 0){
		$('#active_filter_items').removeClass('active');
	}
	currentPage = 1;
}

$(window).scroll(function() {
	if ($(window).scrollTop() >= (($(document).height() - $(window).height()) - 100) ) {
		if(!loading){
			loading = true;
			currentPage++;
			get_next_posts();	
		}
	}
});

function filter_posts(){
	countryArray = [];
	formatArray = [];
	serviceArray = [];
	pTypeArray = [];
	skillsArray = [];
	rosterArray = [];
	topicArray = [];
	
	ajaxJSON = { action : 'do_ajax', fn : 'next_posts', posttype: postType };
	parseFilterArray();
	var urlParameters = '';
	if(filterArray.length > 0){
		urlParameters = '?';
		if(countryArray.length > 0){
			urlParameters += 'country='+countryArray.join(',')+'&';
			ajaxJSON.country = JSON.stringify(countryArray);
		}
		if(formatArray.length > 0){
			urlParameters += 'format='+formatArray.join(',')+'&';
			ajaxJSON.format = JSON.stringify(formatArray);
		}
		if(serviceArray.length > 0){
			urlParameters += 'service='+serviceArray.join(',')+'&';
			ajaxJSON.service = JSON.stringify(serviceArray);
		}
		if(pTypeArray.length > 0){
			urlParameters += 'project_type='+pTypeArray.join(',')+'&';
			ajaxJSON.ptype = JSON.stringify(pTypeArray);
		}
		if(skillsArray.length > 0){
			urlParameters += 'skills='+skillsArray.join(',')+'&';
			ajaxJSON.skills = JSON.stringify(skillsArray);
		}
		if(rosterArray.length > 0){
			urlParameters += 'rosters='+rosterArray.join(',')+'&';
			ajaxJSON.rosters = JSON.stringify(rosterArray);
		}
		if(topicArray.length > 0){
			urlParameters += 'topic='+topicArray.join(',')+'&';
			ajaxJSON.topic = JSON.stringify(topicArray);
		}
		history.replaceState({}, '', urlParameters.slice(0,-1));
	}else{
		var url = window.location.href;
		history.replaceState({}, '', url.split("?")[0]);
	}
	console.log(ajaxJSON);
	
	$.ajax({
		url: ajaxurl,
		data: ajaxJSON,
		//dataType:'json',
		success: function(data) {
			if(data){
				var $items = $(data);
				
				$isotopeContainer.imagesLoaded( function() {
					$isotopeContainer.isotope('remove', $isotopeContainer.isotope('getItemElements'));
					$isotopeContainer.append( $items ).isotope( 'appended', $items ).isotope('layout');
				});
				
				loading = false;
			}else{
				// NO DATA. KEEP IT PAUSED
				if(currentPage == 1){
					$isotopeContainer.isotope('remove', $isotopeContainer.isotope('getItemElements'));
				}
			}
		},
		complete:function(){
		},
		error: function(errorThrown) {
			console.log(errorThrown);
		}
	});
}

function get_next_posts(){
	parseFilterArray();
	$.ajax({
		url: ajaxurl,
		data: {
			'action': 'do_ajax',
			'fn': 'next_posts',
			'posttype': postType,
			'paged': currentPage,
			'country': JSON.stringify(countryArray),
			'format': JSON.stringify(formatArray),
			'service': JSON.stringify(serviceArray),
			'ptype': JSON.stringify(pTypeArray),
			'skills': JSON.stringify(skillsArray),
			'rosters': JSON.stringify(rosterArray),
			'topic': JSON.stringify(topicArray),
		},
		success: function(data) {
			if(data){
				console.log(data);
				var $items = $(data);
				$isotopeContainer.append( $items ).isotope( 'appended', $items );
				loading = false;
			}else{
				// NO DATA. KEEP IT PAUSED
			}
		},
		complete:function(){
		},
		error: function(errorThrown) {
			console.log(errorThrown);
		}
	});
}

function parseFilterArray(){
	$.each(filterArray, function(i, item){
		switch(item.tax){
			case 'country':
				countryArray.push(item.slug);
			break;
			case 'format':
				formatArray.push(item.slug);
			break;
			case 'service':
				serviceArray.push(item.slug);
			break;
			case 'project_type':
				pTypeArray.push(item.slug);
			break;
			case 'skills':
				skillsArray.push(item.slug);
			break;
			case 'roster':
				rosterArray.push(item.slug);
			break;
			case 'topic':
				topicArray.push(item.slug);
			break;
		}
	});
}
function getUrlParameter() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
	if(sURLVariables != ''){
	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');
			var paramArray = sParameterName[1].split(',');
				$.each(paramArray, function(i, item){
					var filterObj = {tax: sParameterName[0], slug: item};
					filterArray.push(filterObj);
				});
				
	        /*if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }*/
	    }
    }
};