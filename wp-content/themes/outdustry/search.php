<?php get_header(); ?>
<section id="content" role="main">
<?php if ( $pCount = get_the_isotope_post_count(array('post_type'=>array('post', 'project', 'roster', 'team'), 's'=>get_search_query())) > 0 ) : ?>
	<header class="header">
	<div id="search">
		<?php global $wp_query; ?>
		<h5 id="search_header_text" class="header_spaced">search: <?php echo $pCount; ?> <?php echo $pCount == 1 ? 'result' : 'results'; ?></h5>
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		    <label>
		        <input type="search" class="header_spaced search-field"
		            placeholder="<?php echo esc_attr_x( '"<?php echo get_search_query(); ?>"', 'placeholder' ) ?>"
		            value="<?php echo get_search_query() ?>" name="s"
		            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" focustext="<?php echo esc_attr_x(qtranxf_use(qtrans_getLanguage(), get_field('search_text_hover', 'option'), false), 'placeholder_focus'); ?>" />
		    </label>
		    <input hidden="true" type="submit" class="search-submit"
		        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
		</form>
	</div>
	</header>
	
	<div id="all_projects" class="all_search">
		<div class="projects_gutter"></div>
			<?php echo get_the_isotope_posts(array('post_type'=>array('post', 'project', 'roster', 'team'), 's'=>get_search_query()), 'search'); ?>
	</div>
		
<?php else : ?>
	<header class="header">
	<div id="search">
		<?php global $wp_query; ?>
		<h3 class="search_not_found"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('search_not_found', 'option'), false); ?> <span><?php echo get_search_query(); ?></span></h3>
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		    <label>
		        <input type="search" class="header_spaced search-field" placeholder="<?php echo esc_attr_x( qtranxf_use(qtrans_getLanguage(), get_field('search_try_again', 'option'), false), 'placeholder' ) ?>" name="s"
		            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" focustext="<?php echo esc_attr_x(qtranxf_use(qtrans_getLanguage(), get_field('search_text_hover', 'option'), false), 'placeholder_focus'); ?>" />
		    </label>
		    <input hidden="true" type="submit" class="search-submit"
		        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
		</form>
	</div>
	</header>
	<h4><?php echo qtranxf_use(qtrans_getLanguage(), get_field('search_not_found_try_these', 'option'), false); ?></h4>
	<div id="all_projects" class="all_search">
		<div class="projects_gutter"></div>
			<?php echo get_the_isotope_posts(array('post_type'=>array('post', 'project'), 'orderby'=>'rand'), 'search'); ?>
	</div>
<?php endif; ?>
</section>
<?php get_footer(); ?>