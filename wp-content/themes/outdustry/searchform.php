<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label>
        <input type="search" class="header_spaced search-field"
            placeholder="<?php echo esc_attr_x( qtranxf_use(qtrans_getLanguage(), get_field('search_text', 'option'), false), 'placeholder' ) ?>"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" focustext="<?php echo esc_attr_x(qtranxf_use(qtrans_getLanguage(), get_field('search_text_hover', 'option'), false), 'placeholder_focus'); ?>" />
    </label>
    <input hidden="true" type="submit" class="search-submit"
        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
</form>