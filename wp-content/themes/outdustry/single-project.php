<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<h6 class="brackets main_page_title">PROJECTS</h6>

<div class="project_container">
	<div class="project_container_inner">
		<div class="project_media">
			<?php if ( has_post_thumbnail() ) { ?>
				<div id="post_featured_image">
		 			<?php the_post_thumbnail(); ?>
	 			</div>
	 		<?php } ?>
		</div>
		<div class="post_info_container">
			<div class="post_info_title">
				<h1 class="entry-title">
					<?php the_title(); ?>
				</h1>
			</div>
			<section class="entry-content">
				<?php 
					$theID = get_the_id();
					$p_artist = get_field('project_artist');
					$services = get_the_terms($theID, 'service');
					$servicesArray = array();
					$formats = get_the_terms($theID, 'format');
					$client = get_field('project_client');
					if(!empty($p_artist)){ ?>
						<h5 class="artist_header italic"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('the_artist', 'option'), false); ?>: <?php print $p_artist; ?></h5>
					<?php } ?>
					<div class="project_terms_boxes">
						<?php if(count($services) > 0 && is_array($services)){ ?>
						<div class="project_terms_box">
							<?php $serviceTax = get_taxonomy('service'); ?>
							<h6 class="project_term_header"><?php echo $serviceTax->label; ?></h6>
							<?php foreach($services as $service){ ?>
								<?php array_push($servicesArray, $service->slug); ?>
								<h6 class="project_term"><a class="brackets" href="<?php echo get_term_link($service->term_id, 'service'); ?> "><?php echo $service->name; ?></a></h6>
							<?php } ?>
						</div>	
						<?php } ?>
						<?php
						if(count($formats) > 0 && is_array($formats)){ ?>
						<div class="project_terms_box">
							<?php $formatTax = get_taxonomy('format'); ?>
							<h6 class="project_term_header"><?php echo $formatTax->label; ?></h6>
							<?php foreach($formats as $format){ ?>
								<h6 class="project_term"><a class="brackets" href="<?php echo get_term_link($format->term_id, 'service'); ?> "><?php echo $format->name; ?></a></h6>
							<?php } ?>
						</div>	
						<?php } ?>
						
						<?php
						$rosters = get_posts( array(
						  'connected_type' => 'roster_to_projects',
						  'connected_items' => get_queried_object(),
						  'nopaging' => true,
						  'suppress_filters' => false
						) );
						if(count($rosters) > 0){ ?>
							<div class="project_terms_box">
								<h6 class="project_term_header"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('roster_post_type', 'option'), false); ?></h6>
								<?php foreach($rosters as $roster){ ?>
									<h6 class="project_term"><a href="<?php echo get_the_permalink($roster->ID); ?>"><?php echo $roster->post_title; ?></a></h6>	
								<?php } ?>
							</div>
						<?php } ?>
						
						<?php if(!empty($client)){ ?>
							<div class="project_terms_box">
								<h6 class="project_term_header"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_client', 'option'), false); ?></h6>
								<h6 class="project_term"><?php echo $client; ?></h6>
							</div>
						<?php } ?>
					</div>
					<?php 
						$project_release_date = get_field('project_release_date');
						$project_licensee = get_field('project_licensee');
						$project_isrc = get_field('project_isrc');
						$project_upc = get_field('project_upc');
						$project_producer = get_field('project_producer');
						$project_composer = get_field('project_composer');
						$project_song_writer = get_field('project_song_writer');
						$project_director = get_field('project_director');
						$project_lead_actors = get_field('project_lead_actors');
						$project_production_cd = get_field('project_production_cd');
						$project_mixing = get_field('project_mixing');
						$project_publishers = get_field('project_publishers');
						$project_mastering = get_field('project_mastering');
						$project_artist = get_field('project_artist'); ?>
						
						<?php if(!empty($project_release_date)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_release_date', 'option'), false); ?></h6>
								<h6><?php echo $project_release_date; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_licensee)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('licensee', 'option'), false); ?></h6>
								<h6><?php echo $project_licensee; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_isrc)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_isrc', 'option'), false); ?></h6>
								<h6><?php echo $project_isrc; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_upc)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_upc', 'option'), false); ?></h6>
								<h6><?php echo $project_upc; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_producer)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_producer', 'option'), false); ?></h6>
								<h6><?php echo $project_producer; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_composer)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_composer', 'option'), false); ?></h6>
								<h6><?php echo $project_composer; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_director)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_director', 'option'), false); ?></h6>
								<h6><?php echo $project_director; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_lead_actors)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_lead_actors', 'option'), false); ?></h6>
								<h6><?php echo $project_lead_actors; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_production_cd)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_production_cd', 'option'), false); ?></h6>
								<h6><?php echo $project_production_cd; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_mixing)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_mixing', 'option'), false); ?></h6>
								<h6><?php echo $project_mixing; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_publishers)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_publishers', 'option'), false); ?></h6>
								<h6><?php echo $project_song_writer; ?></h6>
							</div>
						<?php } ?>
						<?php if(!empty($project_mastering)){ ?>
							<div class="project_info_item">
								<h6><?php echo qtranxf_use(qtrans_getLanguage(), get_field('project_mastering', 'option'), false); ?></h6>
								<h6><?php echo $project_mastering; ?></h6>
							</div>
						<?php } ?>
					<div class="project_all_info">
					</div>
					
			</section>
		</div>
	</div>
	<footer class="footer">
		<?php get_template_part( 'nav', 'below-single' ); ?>
		<div class="more_content_container">
			<h5 class="header_spaced header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('more_projects_header', 'option'), false); ?></h5>
			<div class="more_content">
				<?php echo get_related_content(array('post_type' => 'project', 'posts_per_page' => 3, 'post__not_in' => array($theID), 'tax_query' => array(
							array(
								'taxonomy' => 'service',
								'field'    => 'slug',
								'terms'    => $servicesArray,
							),
						)
					)
				); ?>
			</div>
		</div>
	</footer>
</div>

<?php endwhile; endif; ?>


</section>
<?php get_footer(); ?>