<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<h6 class="brackets main_page_title"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('roster_post_type', 'option'), false); ?></h6>

<div class="project_container">
	<div class="project_container_inner">
		<div class="project_media">
			<?php if ( has_post_thumbnail() ) { ?>
				<div id="post_featured_image">
		 			<?php the_post_thumbnail(); ?>
	 			</div>
	 		<?php } ?>
		</div>
		<div class="post_info_container roster">
			<?php 
				$theID = get_the_id();
				$skills = get_the_terms($theID, 'skills');
				$roles = get_the_terms($theID, 'roles'); 
				$skillsArray = array(); 
				if($skills){
					foreach($skills as $skill){
						array_push($skillsArray, $skill->name);
					}
				}
				$skills = implode(', ', $skillsArray);
				?>
			<div class="post_info">
				<?php if(get_field('title')){ ?>
					<h5><?php echo get_field('title'); ?></h5>
				<?php } ?>
				<h1 class="entry-title">
					<?php the_title(); ?>
				</h1>
				<div class="skills_list">
					<em><?php echo $skills; ?></em>
				</div>
			</div>
			<section class="entry-content">
				<?php the_content(); ?>
				<div class="service_box_posts">
					<div class="service_post_header">
						<a href="<?php echo get_home_url().'/projects/?roster='.basename(get_the_permalink()); ?>">
							<h5 class="header_upper">all projects by <?php the_title(); ?></h5>
							 <div class="arrow_small"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/></div>
						</a>
					</div>
					<h6 class="services_key header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('key_projects', 'option'), false); ?></h6>
					<?php 
					$args = array('post_type' => 'project', 'posts_per_page' => 4, 'connected_type' => 'roster_to_projects', 'connected_items' => get_the_id());
					$projects_query = new WP_Query( $args ); 
					if ( $projects_query->have_posts() ) : ?>
						<?php while ( $projects_query->have_posts() ) : $projects_query->the_post(); ?>
							<?php $formats = get_the_terms(get_the_id(), 'format'); ?>
							<?php $format = $formats[0]; ?>
							<div class="service_post_box">
								<h6 class="service_post_format boldHeader"><?php echo qtranxf_use(qtrans_getLanguage(), $format->name,false); ?></h6>
								<h6><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h6>
							</div>
						<?php endwhile; ?>
					<?php wp_reset_postdata();
						endif; ?>
				</div>
			</section>
		</div>
	</div>
</div>

<?php endwhile; endif; ?>
<footer class="footer">
</footer>
</section>
<?php get_footer(); ?>