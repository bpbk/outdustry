<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php $blog_title = get_the_title( get_option('page_for_posts', true)); ?>
<?php $blog_id = get_the_id(); ?>
<?php $services = get_the_terms(get_the_id(), 'service'); ?>
<?php $servicesArray = array(); ?>
<?php if($services && count($services) > 0){ ?>
	<?php foreach($services as $service){
		array_push($servicesArray, $service->term_id);
	} ?>
<?php } ?>
<h6 class="brackets main_page_title"><?php echo $blog_title; ?></h6>


<?php if ( has_post_thumbnail() ) { ?>
 	<div id="post_featured_image">
	 	<?php the_post_thumbnail(); ?>
 	</div>
 <?php } ?>
<h5 class="entry-date"><span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span></h5>
<div class="post_info_container">
	<div class="post_info_title">
		<h1 class="entry-title">
			<?php the_title(); ?>
		</h1>
	</div>
	<div class="post_info_share">
		<img src="<?php echo get_template_directory_uri(); ?>/images/share.png"/>
	</div>
</div>
<?php $authorID = $post->post_author; ?>
<?php $authorTitle = get_field('title_at_outdustry', 'user_'.$authorID); ?>
<h5 class="boldHeader authorInfo header_upper"><a href="<?php echo get_author_posts_url($authorID); ?>"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('by_author', 'option'), false); ?> <?php echo get_author_name($authorID); ?><?php if ($authorTitle){ echo ', '. $authorTitle;} ?></a></h5>
<section class="entry-content">
	<?php the_content(); ?>
	<?php $tagArray = array(); ?>
	<?php $allTags = get_the_tags(get_the_id()); ?>
	<?php if($allTags && count($allTags) > 0){ ?>
		<div id="tags_container">
			<h5>Tags: </h5>
			<?php foreach($allTags as $tag){ ?>
				<a class="brackets" href="<?php echo get_tag_link($tag); ?>"><?php echo $tag->name; ?></a>
				<?php array_push($tagArray, $tag->term_id); ?>
			<?php } ?>
		</div>
	<?php } ?>
	<!--<?php if($services && count($services) > 0){ ?>
		<div id="related_services_container">
			<h5 class="header_spaced header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('related_services_header', 'option'), false); ?></h5>
			<ul>
			<?php foreach($services as $service){ ?>
				<li><a href="<?php echo get_home_url().'/projects/?service='.$service->slug; ?>"><?php echo $service->name; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	<?php } ?>-->
</section>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>

<?php endwhile; endif; ?>
<footer class="footer">
	<div class="more_content_container">
		<h5 class="header_spaced header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('related_posts_header', 'option'), false); ?></h5>
		<div class="more_content">
<!--
			<?php if(count($servicesArray) < 3){
				$moreServices = get_terms(array('taxonomy'=>'service', 'exclude'=>$servicesArray, 'number' => 3 - count($servicesArray)));
				foreach($moreServices as $anotherService){
					array_push($servicesArray, $anotherService->term_id);
				}
			} ?>
			<?php $relatedServices = get_terms(array('taxonomy'=>'service', 'number'=>3, 'include'=>$servicesArray )); ?>
			<?php foreach($relatedServices as $rServ){ ?>
				<?php $serviceImage = get_template_directory_uri().'/images/placeholder.png'; ?>
				<?php $serviceImageArray = get_field('service_image', $rServ); ?>
				<?php if(!empty($serviceImageArray)){ ?>
					<?php $serviceImage = $serviceImageArray['sizes']['medium']; ?>
				<?php } ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="related_content_container">
						<div class="related_content_image bg_centered" style="background-image:url(<?php echo $serviceImage; ?>)">
							<a href="<?php echo get_home_url().'/projects/?service='.$rServ->slug; ?>"></a>
						</div>
						<div class="related_content">
							<h3 class="entry-title related-content-title">
								<span>
									<a href="<?php echo get_home_url().'/projects/?service='.$rServ->slug; ?>"><?php echo $rServ->name; ?></a>
								</span>
							</h3>
						</div>
					</div>
				</article>
			<?php } ?>
-->
			<?php echo get_related_content(array('post_type' => 'post', 'posts_per_page' => 3, 'post__not_in' => array($blog_id), 'tag__in' => $tagArray ) ); ?>
		</div>
	</div>
</footer>
</section>
<?php get_footer(); ?>