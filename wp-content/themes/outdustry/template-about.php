<?php
	//Template Name: About
?>

<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="page_sub_header">
		<div id="page_sub_header_container">
			<?php $page_title = get_the_title(); ?>
			<h6 class="brackets main_page_title"><?php echo $page_title; ?></h6>
			<div class="page_sub_header_content">
				<?php the_content(); ?>
			</div>
			<!--<div class="outdustry_social">
				<?php if(get_field('instagram_link', 'option')){ ?>
					<div class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_insta_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('instagram_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('facebook_link', 'option')){ ?>
					<div class="social_item social_item_fb" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_fb_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('facebook_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('linkedIn_link', 'option')){ ?>
					<div class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_li_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('linkedIn_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('twitter_link', 'option')){ ?>
					<div class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_twitter_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('twitter_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
				<?php if(get_field('weibo_link', 'option')){ ?>
					<div class="social_item" style="background-image:url(<?php echo get_template_directory_uri().'/images/icon_weibo_black.png'; ?>);">
						<a target="_blank" href="<?php echo get_field('weibo_link', 'option'); ?>"></a>
					</div>
				<?php } ?>
			</div>-->
		</div>
	</div>
	<?php 
	$args = array('role' => 'Author');
	$user_query = new WP_User_Query( $args );
	if ( ! empty( $user_query->results ) ) { ?>
		<div id="about_team">
		<?php foreach ( $user_query->results as $user ) { ?>
		<?php $fullImage = get_field('full_image', 'user_'.$user->ID); ?>
		<?php $outTitle = get_field('title_at_outdustry', 'user_'.$user->ID); ?>
		<?php $headerText = get_field('header_text', 'user_'.$user->ID); ?>
		<?php $excerpt = get_field('author_excerpt', 'user_'.$user->ID); ?>
		<?php $desc = get_the_author_meta('description', $user->ID); ?>
			<article class="author_box">
				<div class="archive_post_container">
					<div class="archive_post_image">
						<div class="archive_post_image_bg bg_centered" style="background-image:url(<?php echo $fullImage['sizes']['large']; ?>)">
							<a href="<?php echo get_author_posts_url( $user->ID ); ?>"></a>
						</div>
					</div>
					<div class="archive_post_content_container">
						<div class="archive_post_content">
							<div class="archive_posts_content_inner">
								<?php if(!empty($outTitle)){ ?>
									<h5 class="outdustry_title"><?php echo $outTitle; ?></h5>
								<?php } ?>
								<?php if(!empty($headerText)){ ?>
									<h2 class="entry-title"><a href="<?php echo get_author_posts_url( $user->ID ); ?>"><?php echo $headerText; ?></a></h2>
								<?php } ?>
								<?php if(!empty($excerpt)){ ?>
									<div class="author_excerpt italic">
										<?php echo $excerpt; ?>
									</div>
								<?php } ?>
								<?php if(!empty($desc)){ ?>
									<p class="author_desc"><?php echo $desc; ?></p>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</article>
		<?php } ?>
	</div>
		<?php } ?>
	<?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>