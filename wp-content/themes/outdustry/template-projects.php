<?php
	//Template Name: Projects
?>

<?php get_header(); ?>
<section id="content" role="main" posttype="project">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="page_sub_header">
		<div id="page_sub_header_container">
			<?php $page_title = get_the_title(); ?>
			<h6 class="brackets main_page_title"><?php echo $page_title; ?></h6>
			<div class="page_sub_header_content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<div id="all_projects_container">
		<h5 class="header_spaced mobile filter_by header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('filter_by', 'option'), false); ?></h5>
		<div id="filter_boxes">
			<div id="filter_box_country" class="filter_box">
				<?php $theTerm = get_taxonomy('country'); ?>
				<div class="filter_box_inner">
					<div class="filter_box_content filter_header" slug="country">
						<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
						<div class="filter_arrow arrow_small">
							<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
						</div>
					</div>
					<?php $terms = get_terms(array('taxonomy' => 'country' )); ?>
						<?php if(count($terms) > 0){ ?>
							<div class="filter_list">
								<?php foreach($terms as $term){ ?>
									<div class="filter_item country_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="country">
										<div class="filter_item_content">
											<div class="filter_checkbox"></div>
											<p><?php echo $term->name; ?></p>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
				</div>
			</div>
			<div id="filter_box_format" class="filter_box">
				<?php $theTerm = get_taxonomy('format'); ?>
				<div class="filter_box_inner">
					<div class="filter_box_content filter_header" slug="format">
						<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
						<div class="filter_arrow arrow_small">
							<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
						</div>
					</div>
					<?php $terms = get_terms(array('taxonomy' => 'format' )); ?>
						<?php if(count($terms) > 0){ ?>
							<div class="filter_list">
								<?php foreach($terms as $term){ ?>
									<div class="filter_item format_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="format">
										<div class="filter_item_content">
											<div class="filter_checkbox"></div>
											<p><?php echo $term->name; ?></p>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
				</div>
			</div>
			<div id="filter_box_service" class="filter_box">
				<?php $theTerm = get_taxonomy('service'); ?>
				<div class="filter_box_inner">
					<div class="filter_box_content filter_header" slug="service">
						<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
						<div class="filter_arrow arrow_small">
							<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
						</div>
					</div>
					<?php $terms = get_terms(array('taxonomy' => 'service' )); ?>
						<?php if(count($terms) > 0){ ?>
							<div class="filter_list">
								<?php foreach($terms as $term){ ?>
									<div class="filter_item service_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="service">
										<div class="filter_item_content">
											<div class="filter_checkbox"></div>
											<p><?php echo $term->name; ?></p>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
				</div>
			</div>
			<div id="filter_box_project_type" class="filter_box">
				<?php $theTerm = get_taxonomy('project_type'); ?>
				<div class="filter_box_inner">
					<div class="filter_box_content filter_header" slug="project_type">
						<h6 class="header_spaced"><?php echo $theTerm->label; ?></h6>
						<div class="filter_arrow arrow_small">
							<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
						</div>
					</div>
					<?php $terms = get_terms(array('taxonomy' => 'project_type' )); ?>
						<?php if(count($terms) > 0){ ?>
							<div class="filter_list">
								<?php foreach($terms as $term){ ?>
									<div class="filter_item post_type_filter filter_item_<?php echo $term->slug; ?>" slug="<?php echo $term->slug; ?>" name="<?php echo $term->name; ?>" tax="project_type">
										<div class="filter_item_content">
											<div class="filter_checkbox"></div>
											<p><?php echo $term->name; ?></p>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
				</div>
			</div>
			<div id="filter_box_roster" class="filter_box">
				<div class="filter_box_inner">
					<div class="filter_box_content filter_header" slug="roster">
						<h6 class="header_spaced"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('roster_post_type', 'option'), false); ?></h6>
						<div class="filter_arrow arrow_small">
							<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/>
						</div>
					</div>
					<?php $rosters = get_posts(array('post_type' => 'roster', 'posts_per_page' => -1)); ?>
						<?php if(count($rosters) > 0){ ?>
							<div class="filter_list">
								<?php foreach($rosters as $roster){ ?>
									<?php $rosterPosts = get_posts( array(
											  'connected_type' => 'roster_to_projects',
											  'connected_items' => $roster,
											) ); ?>
									<?php if(count($rosterPosts) > 0){ ?>
										<div class="filter_item post_type_filter filter_item_<?php echo $roster->post_name; ?>" slug="<?php echo $roster->post_name; ?>" name="<?php echo $roster->post_title; ?>" tax="roster">
											<div class="filter_item_content">
												<div class="filter_checkbox"></div>
												<p><?php echo $roster->post_title; ?></p>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						<?php } ?>
				</div>
			</div>
		</div>
		<div id="active_filter_items">
		</div>
		<div id="all_projects" class="all_projects">
			<div class="projects_gutter"></div>
			<?php 
			
			echo pre_isotope_posts(1, 'project'); ?>
		</div>
	</div>
	<?php endwhile; endif; ?>
<?php get_template_part( 'nav', 'below' ); ?>
</section>
<script>

</script>
<?php get_footer(); ?>