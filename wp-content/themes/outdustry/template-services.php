<?php
	//Template Name: Services
?>

<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="page_sub_header">
		<div id="page_sub_header_container">
			<?php $page_title = get_the_title(); ?>
			<h6 class="brackets main_page_title"><?php echo $page_title; ?></h6>
			<div class="page_sub_header_content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<div id="all_services_container">
	<?php $services = get_terms(array('taxonomy'=>'service', 'hide_empty'=>false)); ?>
	<?php foreach($services as $service){ ?>
		<?php $serviceContent = get_field('service_content', $service); ?>
		<div class="service_box">
			<?php $serviceImage = get_template_directory_uri().'/images/placeholder.png'; ?>
			<?php $serviceImageArray = get_field('service_image', $service); ?>
			<?php if(!empty($serviceImageArray)){ ?>
				<?php $serviceImage = $serviceImageArray['sizes']['medium']; ?>
			<?php } ?>
			<div class="service_box_holder">
				<div class="service_box_image bg_centered" style="background-image:url(<?php echo $serviceImage; ?>)">
				</div>
				<div class="service_box_info">
					<div class="service_box_content">
						<h2><?php echo $service->name; ?></h2>
						<?php if($service->description){ ?>
							<div class="service_excerpt">
								<?php echo $service->description; ?>
							</div>
						<?php } ?>
						<?php if(!empty($serviceContent)){ ?>
							<div class="service_content">
								<?php echo $serviceContent; ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="service_box_posts">
				<div class="service_post_header">
					<a href="<?php echo get_home_url().'/projects/?service='.$service->slug; ?>">
						<h5>all <?php echo $service->name; ?> <?php echo qtranxf_use(qtrans_getLanguage(), get_field('title_projects', 'option'), false); ?></h5>
						<div class="arrow_small"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow_small.png"/></div>
					</a>
				</div>
				<h6 class="services_key header_upper"><?php echo qtranxf_use(qtrans_getLanguage(), get_field('key_projects', 'option'), false); ?></h6>
				<?php 
				$args = array('post_type' => 'project', 'posts_per_page' => 4, 'tax_query' => array(
							array(
								'taxonomy' => 'service',
								'field'    => 'slug',
								'terms'    => $service->slug,
							),
						),
					);
				$projects_query = new WP_Query( $args ); 
				if ( $projects_query->have_posts() ) : ?>
					<?php while ( $projects_query->have_posts() ) : $projects_query->the_post(); ?>
						<?php $formats = get_the_terms(get_the_id(), 'format'); ?>
						<?php $format = $formats[0]; ?>
						<div class="service_post_box">
							<h6 class="service_post_format boldHeader"><?php echo qtranxf_use(qtrans_getLanguage(), $format->name,false); ?></h6>
							<h6><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h6>
						</div>
					<?php endwhile; ?>
				<?php wp_reset_postdata();
					endif; ?>
			</div>
		</div>
	<?php } ?>
	
	</div>
	<?php endwhile; endif; ?>
<?php get_template_part( 'nav', 'below' ); ?>
</section>
<?php get_footer(); ?>